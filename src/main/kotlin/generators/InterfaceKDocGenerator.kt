package generators

import org.jetbrains.kotlin.psi.KtClass
import utils.getDefaultCommentFromName

/**
 * Interface Kotlin documentation generator.
 *
 * @property klass [KtClass]
 * @constructor Create [InterfaceKDocGenerator]
 */
class InterfaceKDocGenerator(private val klass: KtClass) : KDocGenerator {
    override fun getGeneratedComment(): String = StringBuilder()
        .appendLine("/**")
            .appendLine("* ${klass.name.getDefaultCommentFromName()}")
            .appendLine("*/")
            .toString()
}